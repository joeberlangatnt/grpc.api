﻿using System;
using System.Threading.Tasks;
using Grpc.Api.Authorise.Helpers;
using Grpc.Api.Shared.Contracts;
using Grpc.Api.Shared.Entities;
using Grpc.Api.Shared.Enums;
using Grpc.Api.Shared.Models;
using Microsoft.AspNetCore.Authorization;

namespace Grpc.Api.Authorise.Services
{
    [AllowAnonymous]
    public class AuthoriseService : IAuthoriseService
    {
        private static readonly User _user = new User { Username = "admintest", FirstName = "Joe", LastName = "Berlanga", Id = 1, Password = "admintest", Role = Role.ADMIN };

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest model)
        {
            //go to database here and check credentials etc
            var user = _user;

            // return null if user not found - obviously it always is right now
            if (user == null) return null;

            // authentication successful so generate jwt token and return it
            var token = TokenHelpers.GenerateJwtToken(user);

            //this token is a refresh token thats assigned to a user when they successfully login, with this the user can call the refresh token method 
            //to get themselves a new JWT token. This way they dont need to get authenticated every X minutes (expiry).
            var refreshToken = TokenHelpers.GenerateRefreshToken();

            var response = new AuthenticateResponse { User = user, Token = token.token, RefreshToken = refreshToken, Expiry = token.expiry };

            //store token against the specific user so we can validate it later
            user.RefreshToken = refreshToken;
            user.Token = token.token;
            user.TokenExpiry = DateTime.UtcNow.AddDays(1);

            return await Task.FromResult(response);
        }

        //we cant call this method over the gRPC api because of the params, objects we send must have a data contract
        public async Task<AuthenticateResponse> RefreshToken(string token, string refreshToken)
        {           
            var response = await this.ValidateRefreshToken(token, refreshToken);

            return response;
        }

        public async Task<AuthenticateResponse> RefreshToken(Token token)
        {
            var response = await this.ValidateRefreshToken(token.JWTBearerToken, token.RefreshToken);

            return response;
        }

        private async Task<AuthenticateResponse> ValidateRefreshToken(string token, string refreshToken)
        {
            //check if token exists and if refresh token exists and its not expired
            var user = _user.Token == token && _user.RefreshToken == refreshToken && DateTime.UtcNow < _user.TokenExpiry ? _user : null;

            //if token is invalid
            if (user == null)
            {
                return null;
            }

            //if token is valid generate new tokens
            var newRefreshToken = TokenHelpers.GenerateRefreshToken();
            var newToken = TokenHelpers.GenerateJwtToken(user);

            user.RefreshToken = refreshToken;
            user.TokenExpiry = DateTime.UtcNow.AddDays(1);
            user.Token = newToken.token;
            return await Task.FromResult(new AuthenticateResponse { RefreshToken = newRefreshToken, Token = newToken.token, User = user, Expiry = newToken.expiry });
        }
    }
}
