﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Grpc.Api.Shared.Entities;
using Microsoft.IdentityModel.Tokens;

namespace Grpc.Api.Authorise.Helpers
{
    internal static class TokenHelpers
    {
        internal static string GenerateRefreshToken(int size = 64)
        {
            var randomNumber = new byte[size];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }

        internal static (string token, DateTime expiry) GenerateJwtToken(User user)
        {
            using var rsa = RSA.Create();
            rsa.ImportRSAPrivateKey(
                source: Convert.FromBase64String(GetPrivateKey()),
                bytesRead: out _
                );

            var signingCredentials = new SigningCredentials(
                key: new RsaSecurityKey(rsa),
                algorithm: SecurityAlgorithms.RsaSha256
            );

            var expiryDateTime = DateTime.UtcNow.AddMinutes(1);

            var jwt = new JwtSecurityToken(
                audience: "jwt-test",
                issuer: "jwt-test",
                claims: new Claim[] { 
                    new Claim(nameof(user.Id), user.Id.ToString()),
                    new Claim(nameof(user.FirstName), user.FirstName),
                    new Claim(nameof(user.LastName), user.LastName),
                    new Claim(nameof(user.Username), user.Username),
                    new Claim(ClaimTypes.Role, user.Role.ToString()),
                    //not needed but I want to check it in the client to make sure its updating correctly
                    new Claim(ClaimTypes.Expiration, expiryDateTime.ToString())
                },
                expires: expiryDateTime,
                signingCredentials: signingCredentials
            );

            var token = new JwtSecurityTokenHandler().WriteToken(jwt);

            return (token, expiryDateTime);
        }

        private static string GetPrivateKey()
        {
            return File.ReadAllText("RSAPrivateKey.txt");
        }
    }
}
