﻿using System;
using System.IO;
using System.Security.Cryptography;
using Grpc.Api.Authorise.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using ProtoBuf.Grpc.Server;

namespace Grpc.Api.Authorise
{
    public class Startup
    {
        private readonly string _allowedCORSOrigins = "accessControlPolicy";

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCodeFirstGrpc();

            //add cors policy middleware - basically urls we want to allow to call this app
            services.AddCors(options => {
                options.AddPolicy(name: _allowedCORSOrigins, builder =>
                {
                    builder.WithOrigins("https://localhost:44307")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseGrpcWeb();
            app.UseCors(_allowedCORSOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<AuthoriseService>().EnableGrpcWeb();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}
