﻿using System;
using System.Security.Claims;
using Grpc.Api.Shared.Entities;
using Grpc.Api.Shared.Enums;

namespace Grpc.Api.Client.Blazor.Helpers
{
    public static class ClaimsExtension
    {
        public static User GetUser(this ClaimsPrincipal claim)
        {
            var user = new User();
            user.Id = int.Parse(claim.FindFirst(nameof(user.Id)).Value);
            user.FirstName = claim.FindFirst(nameof(user.FirstName)).Value;
            user.LastName = claim.FindFirst(nameof(user.LastName)).Value;
            user.Username = claim.FindFirst(nameof(user.Username)).Value;

            var expiry = claim.FindFirst(ClaimTypes.Expiration).Value;

            if(Enum.TryParse<Role>(claim.FindFirst(ClaimTypes.Role).Value, out var role))
            {
                user.Role = role;
            }

            return user;
        }
    }
}
