﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Grpc.Api.Shared.Models;
using Microsoft.AspNetCore.Components.Authorization;

namespace Grpc.Api.Client.Blazor.AuthenticationStateProvider
{
    class TokenAuthenticationStateProvider : Microsoft.AspNetCore.Components.Authorization.AuthenticationStateProvider
    {
        private readonly ILocalStorageService _localStore;

        private readonly Api.Shared.Contracts.IAuthoriseService _authoriseService;

        public TokenAuthenticationStateProvider(ILocalStorageService localStore, Api.Shared.Contracts.IAuthoriseService authoriseService)
        {
            _localStore = localStore;
            _authoriseService = authoriseService;
        }

        public async Task SetTokenAsync(Token token, bool refresh = true)
        {
            if (token == null)
            {
                await _localStore.RemoveItemAsync("authToken");
            }
            else
            {
                await _localStore.SetItemAsync("authToken", token);
            }

            if (refresh)
            {
                NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
            }
        }

        public async Task<Token> GetTokenAsync()
        {
            if (!await _localStore.ContainKeyAsync("authToken"))
            {
                return null;
            }

            var token = await _localStore.GetItemAsync<Token>("authToken");

            if (token.Expiry > DateTime.UtcNow)
            {
                return token;
            }
            else if (!string.IsNullOrEmpty(token.RefreshToken))
            {
                try
                {
                    var response = await _authoriseService.RefreshToken(token.JWTBearerToken, token.RefreshToken);

                    if (response != null)
                    {
                        var newToken = new Token(response.Token, response.RefreshToken, response.Expiry);

                        //we want to keep calling our services without the page rendering again
                        await SetTokenAsync(newToken, false);

                        return newToken;
                    }
                }
                catch
                {
                    //TODO handling
                    //something happened
                }
            }

            await SetTokenAsync(null);

            return null;
        }


        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var token = await GetTokenAsync();

            if (token == null)
            {
                return new AuthenticationState(new ClaimsPrincipal());
            }

            var identity = new ClaimsIdentity(ParseClaimsFromJwt(token.JWTBearerToken), "jwt");
            return new AuthenticationState(new ClaimsPrincipal(identity));
        }

        private static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);

            return token.Claims;
        }
    }
}
