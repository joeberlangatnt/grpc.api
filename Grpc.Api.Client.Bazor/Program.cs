using System;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Grpc.Api.Client.Blazor.AuthenticationStateProvider;
using Grpc.Api.Shared.Contracts;
using Grpc.Net.Client;
using Grpc.Net.Client.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ProtoBuf.Grpc.Client;

namespace Grpc.Api.Client.Blazor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddTransient(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            builder.Services.AddScoped<IAuthoriseService>(services =>
            {
                var httpClient = new HttpClient(new GrpcWebHandler(GrpcWebMode.GrpcWeb, new HttpClientHandler()));
                var channel = GrpcChannel.ForAddress("https://localhost:50000/", new GrpcChannelOptions { HttpClient = httpClient });

                return channel.CreateGrpcService<IAuthoriseService>();
            });

            builder.Services.AddScoped<IUserService>(services =>
            {
                var handler = new TokenHttpInnerHandler(services.GetService<TokenAuthenticationStateProvider>());

                var httpClient = new HttpClient(new GrpcWebHandler(GrpcWebMode.GrpcWeb, handler));
                var channel = GrpcChannel.ForAddress("https://localhost:50001/", new GrpcChannelOptions { HttpClient = httpClient });

                return channel.CreateGrpcService<IUserService>();
            });

            builder.Services.AddScoped<TokenAuthenticationStateProvider>();
            builder.Services.AddScoped<Microsoft.AspNetCore.Components.Authorization.AuthenticationStateProvider>(provider => provider.GetRequiredService<TokenAuthenticationStateProvider>());
            
            builder.Services.AddBlazoredLocalStorage();

            builder.Services.AddApiAuthorization();

            await builder.Build().RunAsync();
        }
    }
}
