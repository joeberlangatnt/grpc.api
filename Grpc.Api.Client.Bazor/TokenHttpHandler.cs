﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Api.Client.Blazor.AuthenticationStateProvider;

namespace Grpc.Api.Client.Blazor
{
    internal class TokenHttpInnerHandler : DelegatingHandler
    {
        private readonly TokenAuthenticationStateProvider _authenticationStateProvider;

        public TokenHttpInnerHandler(TokenAuthenticationStateProvider authenticationStateProvider)
        {
            this._authenticationStateProvider = authenticationStateProvider;
            InnerHandler = new HttpClientHandler();
        }
        
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var token = await _authenticationStateProvider.GetTokenAsync();

            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token?.JWTBearerToken);

            var response = await base.SendAsync(request, cancellationToken);

            return response;
        }
    }
}
