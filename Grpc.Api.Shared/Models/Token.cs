﻿using Newtonsoft.Json;
using ProtoBuf;
using System;

namespace Grpc.Api.Shared.Models
{
    [ProtoContract]
    public class Token
    {
        public Token() { }

        public Token(string jWTBearerToken, string refreshToken, DateTime? expiry = null)
        {
            JWTBearerToken = jWTBearerToken;
            RefreshToken = refreshToken;
            Expiry = expiry;

        }

        [JsonProperty]
        [ProtoMember(1)]
        public string JWTBearerToken { get; set; }

        [JsonProperty]
        [ProtoMember(2)]
        public string RefreshToken { get; set; }

        [JsonProperty]
        [ProtoMember(3)]
        public DateTime? Expiry { get; set; }
    }
}
