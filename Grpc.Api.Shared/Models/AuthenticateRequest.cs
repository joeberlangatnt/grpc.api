﻿using ProtoBuf;

namespace Grpc.Api.Shared.Models
{
    [ProtoContract]
    public class AuthenticateRequest
    {
        [ProtoMember(1)]
        public string Username { get; set; }

        [ProtoMember(2)]
        public string Password { get; set; }
    }
}
