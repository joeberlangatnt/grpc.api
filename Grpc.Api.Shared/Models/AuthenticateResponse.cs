﻿using Grpc.Api.Shared.Entities;
using ProtoBuf;
using System;

namespace Grpc.Api.Shared.Models
{
    [ProtoContract]
    public class AuthenticateResponse
    {
        [ProtoMember(1)]
        public User User { get; set; }

        [ProtoMember(2)]
        public string Token { get; set; }

        [ProtoMember(3)]
        public string RefreshToken { get; set; }

        [ProtoMember(4)]
        public DateTime Expiry { get; set; }

    }
}
