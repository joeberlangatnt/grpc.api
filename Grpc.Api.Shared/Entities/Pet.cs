﻿namespace Grpc.Api.Shared.Entities
{
    public class Pet
    {
        public string PetId { get; set; }

        public string Name { get; set; }
    }
}
