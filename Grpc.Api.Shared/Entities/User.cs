﻿using Grpc.Api.Shared.Enums;
using ProtoBuf;
using System;

namespace Grpc.Api.Shared.Entities
{
    [ProtoContract]
    public class User
    {
        [ProtoMember(1)]
        public int Id { get; set; }

        [ProtoMember(2)]
        public string FirstName { get; set; }

        [ProtoMember(3)]
        public string LastName { get; set; }

        [ProtoMember(4)]
        public string Username { get; set; }

        [ProtoMember(5)]
        public Role Role { get; set; }

        //storing here because I have no DB
        [ProtoIgnore]
        public string Token { get; set; }

        [ProtoIgnore]
        public string RefreshToken { get; set; }

        [ProtoIgnore]
        public DateTime? TokenExpiry { get; set; }

        [ProtoIgnore]
        public string Password { get; set; }
    }
}
