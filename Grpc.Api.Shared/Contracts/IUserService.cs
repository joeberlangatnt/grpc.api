﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Grpc.Api.Shared.Entities;
using Grpc.Api.Shared.Models;
using ProtoBuf.Grpc;

namespace Grpc.Api.Shared.Contracts
{
    [ServiceContract]
    public interface IUserService
    {
        Task<User> GetAll(CallContext context = default);

        Task<User> GetAllAdmin(CallContext context = default);
    }
}
