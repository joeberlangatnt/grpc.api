﻿using Grpc.Api.Shared.Models;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Grpc.Api.Shared.Contracts
{
    [ServiceContract]
    public interface IAuthoriseService
    {
        Task<AuthenticateResponse> Authenticate(AuthenticateRequest model);

        //you cant do this, you need to pass objects with proto attributes
        Task<AuthenticateResponse> RefreshToken(string token, string refreshToken);

        Task<AuthenticateResponse> RefreshToken(Token token);
    }
}
