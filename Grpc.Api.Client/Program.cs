﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Api.Shared.Contracts;
using Grpc.Api.Shared.Models;
using Grpc.Core;
using Grpc.Net.Client;
using ProtoBuf.Grpc;
using ProtoBuf.Grpc.Client;

namespace Grpc.Api.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var authoriseChannel = GrpcChannel.ForAddress("https://localhost:50000/");
            var authoriseService = authoriseChannel.CreateGrpcService<IAuthoriseService>();
            var adminUser = await authoriseService.Authenticate(new Shared.Models.AuthenticateRequest() { Password = "admintest", Username = "admintest" });

            using var userChannel = GrpcChannel.ForAddress("https://localhost:50001/");
            var userService = userChannel.CreateGrpcService<IUserService>();

            var context = GetCallContext(adminUser.Token);

            var userCall = await userService.GetAll(context);

            Console.WriteLine($"{userCall.Username}Has access to the API's");

            var adminUsers = userService.GetAllAdmin(context);

            Console.WriteLine(userCall.Username + " Has access to the Admin secured API's also");

            //token should expire after 1 minutes
            Thread.Sleep(TimeSpan.FromMinutes(1));

            try
            {
                await userService.GetAll(context);
            }
            catch (RpcException e)
            {
                Console.WriteLine("As we expected the token has timed out and therefore we now get unauthenticated");
            }

            Console.WriteLine("We will use the refresh token in this case");

            /*
             * service will check the token was assigned to the user, if it was it will check the refresh token exists and then hasn't expired
               this way we dont need to reauthenticate we can simple just get a new JWT and refresh token
               TODO protobuf needs an object so we can put some attributers on the fields so it knows how to serialize
            */

            AuthenticateResponse newTokens;

            try
            {
                newTokens = await authoriseService.RefreshToken(adminUser.Token, adminUser.RefreshToken);
            }
            catch (Exception)
            {
                Console.WriteLine("Another expected exception got thrown because we didnt pass an object with a data contract, so protobuf doesn't know how to handle the data");
            }

            //this time lets call the end point that has a Token type as a parameter
            newTokens = await authoriseService.RefreshToken(new Token(adminUser.Token, adminUser.RefreshToken));

            context = GetCallContext(newTokens.Token);

            var someUsers = await userService.GetAll(context);

            Console.WriteLine("press return to exit");
            Console.Read();          
        }

        private static CallContext GetCallContext(string bearerToken)
        {
            var headers = new Metadata
            {
                { "Authorization", $"Bearer {bearerToken}" }
            };

            return new CallOptions(headers);
        }
    }
}
