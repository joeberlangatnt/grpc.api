﻿using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Components.Templates
{
    public partial class TableTemplate<T>
    {
        [Parameter]
        public RenderFragment TableHeader { get; set; }

        [Parameter]
        public RenderFragment<T> RowTemplate { get; set; }

        [Parameter]
        public IReadOnlyList<T> Items { get; set; }

        [Parameter]
        public EventCallback<TableTemplateProperties> OnPaginate { get; set; }

        private TableTemplateProperties _tableTemplateProperties;

        protected override Task OnInitializedAsync()
        {
            _tableTemplateProperties = new TableTemplateProperties();

            return base.OnInitializedAsync();
        }

        private async void NextPage()
        {
            if (Items.Count < _tableTemplateProperties.PageSize)
            {
                return;
            }

            _tableTemplateProperties.Next();

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }

        private async void PreviousPage() 
        {
            if (_tableTemplateProperties.PageNumber <= 1)
            {
                return;
            }

            _tableTemplateProperties.Previous();

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }

        private async void PageResize()
        {
            if (Items.Count < _tableTemplateProperties.PageSize)
            {
                return;
            }

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }
    }

    public class TableTemplateProperties
    {
        internal void Next()
        {
            this.PageNumber++;
        }

        internal void Previous() 
        {
            this.PageNumber--;
        }

        internal void SetPageSize(int pageSize)
        {
            this.PageSize = pageSize;
        }

        public int PageNumber { get; private set; } = 1;

        public int PageSize { get; private set; } = 10;

        public int Offset => PageSize * PageNumber - PageNumber;
    }
}
