﻿using ComponentLibrary.Helpers;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Threading.Tasks;

namespace ComponentLibrary.Templates
{
    public partial class PopupTemplate
    {
        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public RenderFragment Content { get; set; }

        [Parameter]
        public bool ShowCancel { get; set; } = true;

        [Parameter]
        public string CancelText { get; set; } = "Cancel";

        [Parameter]
        public string OkText { get; set; } = "Ok";

        [Parameter]
        public EventCallback OnOk { get; set; }

        [Parameter]
        public EventCallback OnCancel { get; set; }

        private string Id { get; set; }

        protected override Task OnInitializedAsync()
        {
            this.Id = StringHelpers.GenerateRandomString();

            return base.OnInitializedAsync();
        }

        internal string Size
        {
            get
            {
                switch (PopupSize)
                {
                    case PopupSize.SMALL:
                        return "sm";
                    case PopupSize.MEDIUM:
                        return "md";
                    case PopupSize.LARGE:
                        return "lg";
                    default:
                        return "md";
                }
            }
        }

        private async Task Ok()
        {
            await HidePopup();

            await OnOk.InvokeAsync(null);
        }

        private async Task Cancel()
        {
            await HidePopup();

            await OnCancel.InvokeAsync(null);
        }

        public async Task Show()
        {
            await ShowPopup();
        }

        [Parameter]
        public PopupSize PopupSize { get; set; }

        internal async Task ShowPopup()
        {
            await jsRuntime.InvokeVoidAsync("OpenPopup", this.Id);
        }

        internal async Task HidePopup()
        {
            await jsRuntime.InvokeVoidAsync("ClosePopup", this.Id);
        }
    }

    public enum PopupSize
    {
        SMALL,
        MEDIUM,
        LARGE
    }
}
