﻿using ComponentLibrary.Models;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComponentLibrary.Templates
{
    public partial class TableTemplate<T>
    {
        [Parameter]
        public RenderFragment TableHeader { get; set; }

        [Parameter]
        public RenderFragment<T> RowTemplate { get; set; }

        [Parameter]
        public IEnumerable<T> Items { get; set; }

        [Parameter]
        public int TotalRecords { get; set; }

        [Parameter]
        public int PageSize { get; set; }

        [Parameter]
        public EventCallback<TableTemplateProperties> OnPaginate { get; set; }

        private TableTemplateProperties _tableTemplateProperties;

        //this will run once on init
        protected override Task OnInitializedAsync()
        {
            _tableTemplateProperties = new TableTemplateProperties(this.TotalRecords, this.PageSize);

            var count = Items.Count();

            if (count > _tableTemplateProperties.PageSize)
            {
                _tableTemplateProperties.SetPageSize(count);
            }

            return base.OnInitializedAsync();
        }

        //this will run every time params change
        protected override Task OnParametersSetAsync()
        {
            _tableTemplateProperties.SetTotalRecords(this.TotalRecords);

            return base.OnParametersSetAsync();
        }

        //this is here so if we change the data we can set the page back to one
        public async Task SetToFirstPage()
        {
            _tableTemplateProperties.SetPage(1);

            await Task.CompletedTask;
        }

        private async Task NextPage()
        {
            if (_tableTemplateProperties.PageNumber == _tableTemplateProperties.TotalPages)
            {
                return;
            }

            _tableTemplateProperties.Next();

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }

        private async Task PreviousPage()
        {
            if (_tableTemplateProperties.PageNumber <= 1)
            {
                return;
            }

            _tableTemplateProperties.Previous();

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }

        private async Task PageResize(int pageSize)
        {
            if (TotalRecords < pageSize)
            {
                return;
            }

            _tableTemplateProperties.SetPageSize(pageSize);

            _tableTemplateProperties.SetPage(1);

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }

        private async Task First()
        {
            _tableTemplateProperties.SetPage(1);

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }

        private async Task Last()
        {
            _tableTemplateProperties.Last();

            await OnPaginate.InvokeAsync(_tableTemplateProperties);
        }
    }    
}
