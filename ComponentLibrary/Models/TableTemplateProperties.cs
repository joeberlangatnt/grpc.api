﻿using System;

namespace ComponentLibrary.Models
{
    public class TableTemplateProperties
    {
        public TableTemplateProperties(int totalRecords, int pageSize)
        {
            this.PageSize = pageSize;
            this.TotalRecords = totalRecords;
        }

        internal void Next()
        {
            this.PageNumber++;
        }

        internal void Previous()
        {
            this.PageNumber--;
        }

        internal void SetTotalRecords(int totalRecords)
        {
            this.TotalRecords = totalRecords;
        }

        internal void SetPage(int page)
        {
            this.PageNumber = page;
        }

        internal void Last()
        {
            this.PageNumber = this.TotalPages;
        }

        internal void SetPageSize(int pageSize)
        {
            this.PageSize = pageSize;
        }

        public int TotalRecords { get; private set; }

        public int PageNumber { get; private set; } = 1;

        public int PageSize { get; private set; } = 10;

        public int TotalPages => (int)Math.Ceiling((double)(TotalRecords / PageSize)) + 1;

        public int Limit
        {
            get
            {
                var itemsLeft = TotalRecords - this.Offset;

                if (itemsLeft < this.PageSize)
                {
                    return itemsLeft;
                }

                return this.PageSize;
            }
        }

        public int Offset => PageSize * PageNumber - PageSize;
    }
}
