﻿window.OpenPopup = (id) => {
    $('#' + id).modal('show');
}

window.ClosePopup = (id) => {
    $('#' + id).modal('hide');
}
