using System.Text;
using Grpc.Api.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using ProtoBuf.Grpc.Server;
using Grpc.Api.Authorization;
using Microsoft.AspNetCore.Authorization;
using Grpc.Api.Services;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Grpc.Api
{
    public class Startup
    {
        private readonly string _allowedCORSOrigins = "accessControlPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            services.AddCodeFirstGrpc();

            //add cors policy middleware - basically urls we want to allow to call this app
            services.AddCors(options => {
                options.AddPolicy(name: _allowedCORSOrigins, builder =>
                {                   
                    builder.WithOrigins("https://localhost:44307")
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();  
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddSingleton<RsaSecurityKey>(provider =>
            {
                var rsa = RSA.Create();
                rsa.ImportRSAPublicKey(
                    source: Convert.FromBase64String(File.ReadAllText("RSAPublicKey.txt")),
                    bytesRead: out _
                );

                return new RsaSecurityKey(rsa);
            });

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })               
                .AddJwtBearer(x =>
                {
                    x.IncludeErrorDetails = true;
                    //use our key to check the tokens
                    SecurityKey rsa = services.BuildServiceProvider().GetRequiredService<RsaSecurityKey>();
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKey = rsa,
                        ValidAudience = "jwt-test",
                        ValidIssuer = "jwt-test",
                        RequireSignedTokens = true,
                        RequireExpirationTime = true, // <- JWTs are required to have "exp" property set
                        ValidateLifetime = true, // <- the "exp" will be validated
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ClockSkew = TimeSpan.Zero
                    };
                });

            //adding admin as a policy so only users with admin role can access certain resources
            services.AddAuthorization(options => {
                options.AddPolicy("Admin", policy => policy.Requirements.Add(new RoleAuthorization(Shared.Enums.Role.ADMIN)));
            });

            services.AddSingleton<IAuthorizationHandler, RoleAuthorizationHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseGrpcWeb(new GrpcWebOptions { DefaultEnabled = true });
            app.UseCors(_allowedCORSOrigins);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<UserService>();
                
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}
