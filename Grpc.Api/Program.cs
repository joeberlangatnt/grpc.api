using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;
using System.Net;

namespace Grpc.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            //configure kestrel to use port 50001 and use HTTPS (default dev cert)
            .UseKestrel(options =>
            {
                //loopback basically means localhost
                options.Listen(IPAddress.Loopback, 50001, ListenOptions =>
                {
                    //uses default cert
                    ListenOptions.UseHttps();
                });
            })
            .UseStartup<Startup>();
    }
}