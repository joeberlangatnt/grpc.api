﻿using Grpc.Api.Helpers;
using Grpc.Api.Shared.Enums;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace Grpc.Api.Authorization
{
    internal class RoleAuthorizationHandler : AuthorizationHandler<RoleAuthorization>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleAuthorization roleAuthorization)
        {
            var user = context.User.GetUser();

            if (user.Role == roleAuthorization.Role)
            {
                context.Succeed(roleAuthorization);
            }

            return Task.CompletedTask;
        }
    }

    internal class RoleAuthorization : IAuthorizationRequirement
    {
        internal Role Role { get; set; }

        public RoleAuthorization(Role role) 
        {
            this.Role = role;
        }
    }
}
