﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Api.Helpers;
using Grpc.Api.Shared.Contracts;
using Grpc.Api.Shared.Entities;
using Grpc.Api.Shared.Enums;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using ProtoBuf.Grpc;

namespace Grpc.Api.Services
{
    [Authorize]
    public class UserService : IUserService
    {       
        internal static List<User> _users = new List<User>
        {
            new User { Id = 1, FirstName = "AdminTest", LastName = "User", Username = "admintest", Password = "admintest", Role = Role.ADMIN },
            new User { Id = 2, FirstName = "Basic", LastName = "User", Username = "basicuser", Password = "test", Role = Role.BASIC}
        };
       
        public async Task<User> GetAll(CallContext context = default)
        {
            //can parse user from token using this extension - we dont need to go to database for user context we can store in token
            var user = context.ServerCallContext.GetHttpContext().User.GetUser();

            switch (user.Role)
            {
                case Role.ADMIN:
                    //do something only admins can do
                    break;
                case Role.MANAGER:
                    //do something managers can do
                    break;
                case Role.BASIC:
                    //do something basic user can do
                    break;
                default:
                    break;
            }

            return await Task.FromResult(_users.FirstOrDefault());
        }

        [Authorize("Admin")]
        public async Task<User> GetAllAdmin(CallContext context)
        {
            return await Task.FromResult(_users.FirstOrDefault());
        }
    }
}
