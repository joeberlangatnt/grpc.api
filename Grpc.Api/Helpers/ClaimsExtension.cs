﻿using System;
using System.Security.Claims;
using Grpc.Api.Shared.Entities;
using Grpc.Api.Shared.Enums;

namespace Grpc.Api.Helpers
{
    public static class ClaimsExtension
    {
        internal static User GetUser(this ClaimsPrincipal claim)
        {
            var user = new User();
            //parsing the fields we set in the above method back into a user object
            user.Id = int.Parse(claim.FindFirstValue(nameof(user.Id)));
            user.FirstName = claim.FindFirstValue(nameof(user.FirstName));
            user.LastName = claim.FindFirstValue(nameof(user.LastName));
            user.Username = claim.FindFirstValue(nameof(user.Username));
            user.Role = Enum.Parse<Role>(claim.FindFirstValue(ClaimTypes.Role));

            return user;
        }
    }
}
